'use strict';

module.exports = function(grunt) {
  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({
    // Project settings
    config: {
      // Configurable paths
      app: 'app',
      stylesFolder: 'styles',
      scriptsFolder: 'scripts',
      dist: 'dist'
    },
    // Watches files for changes and runs tasks based on the changed files
    watch: {
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.app %>/*.html',
          '<%= config.app %>/<%= config.scriptsFolder %>/**/*.js',
          '<%= config.app %>/<%= config.stylesFolder %>/**/*.scss',
        ],
        tasks: ['devCompile']
      }
    },
    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        livereload: 35729,
        hostname: 'localhost'
      },
      proxies: [{
        context: '/api',
        host: 'localhost',
        port: 8081
      }],
      livereload: {
        options: {
          open: true,
          base: [
            '.tmp',
            '<%= config.app %>'
          ],
          middleware: function(connect, options) {
            var middlewares = [];

            if (!Array.isArray(options.base)) {
              options.base = [options.base];
            }

            //Setup the proxy
            middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

            //Serve static files
            options.base.forEach(function(base) {
              middlewares.push(connect.static(base));
            });

            return middlewares;

          }
        }
      },
      distServe: {
        options: {
          keepalive: true,
          open: true,
          base: [
            '<%= config.dist %>'
          ],
          middleware: function(connect, options) {
            var middlewares = [];

            if (!Array.isArray(options.base)) {
              options.base = [options.base];
            }

            //Setup the proxy
            middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

            //Serve static files
            options.base.forEach(function(base) {
              middlewares.push(connect.static(base));
            });

            return middlewares;

          }
        }
      },
      dist: {
        options: {
          keepalive: false,
          open: false,
          base: [
            '<%= config.dist %>'
          ],
          middleware: function(connect, options) {
            var middlewares = [];

            if (!Array.isArray(options.base)) {
              options.base = [options.base];
            }

            //Setup the proxy
            middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

            //Serve static files
            options.base.forEach(function(base) {
              middlewares.push(connect.static(base));
            });

            return middlewares;

          }
        }
      },
      showFlowTests: {
        options: {
          keepalive: true,
          open: true,
          base: [
            '<%= config.dist %>/test-results'
          ]
        }
      }
    },

    copy: {
      devSass: {
        expand: true,
        cwd: 'app/<%= config.stylesFolder %>/',
        src: '**',
        dest: '<%= config.stylesFolder %>/'
      },
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '*.html',
            'views/{,*/}*.html',
            'fonts/*'
          ]
        }]
      },
      docSass: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>/<%= config.stylesFolder %>',
          dest: '.tmp/doc',
          src: [
            'styleguide.md'
          ]
        }]
      },
    },

    rename: {
      moveSCSS: {
        src: '<%= config.stylesFolder %>',
        dest: '.tmp/<%= config.stylesFolder %>'
      },
      moveResultFlow: {
        src: 'test-results',
        dest: '<%= config.dist %>/test-results'
      }
    },

    clean: {
      dev: ['.tmp', '<%= config.stylesFolder %>'],
      dist: ['<%= config.dist %>']
    },

    sass: { // task
      dist: { // target
        options: {
          outputStyle: 'compressed'
        },
        files: { // dictionary of files
          '<%= config.dist %>/<%= config.stylesFolder %>/main.css': '<%= config.app %>/<%= config.stylesFolder %>/main.scss' // 'destination': 'source'
        }
      },
      dev: { // another target
        options: { // dictionary of render options
          sourceMap: './main.css.map'
        },
        files: {
          '<%= config.stylesFolder %>/main.css': '<%= config.stylesFolder %>/main.scss'
        }
      },
      doc: { // another target
        options: { // dictionary of render options
          sourceMap: false
        },
        files: {
          '.tmp/doc/main.css': '<%= config.app %>/<%= config.stylesFolder %>/main.scss'
        }
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= config.app %>/scripts/{,*/}*.js'
      ],

      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },
    // Automatically inject Bower components into the app
    wiredep: {
      target: {
        src: ['<%= config.app %>/*.html'],
        ignorePath: '<%= config.app %>/'
      }

    },
    useminPrepare: {
      html: '<%= config.app %>/index.html',
      options: {
        dest: '<%= config.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= config.dist %>/images'
        }]
      }
    },
    usemin: {
      html: ['<%= config.dist %>/{,*/}*.html']
    },
    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true,
          removeOptionalTags: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.dist %>',
          src: ['*.html', 'views/{,*/}*.html'],
          dest: '<%= config.dist %>'
        }]
      }
    },
    run: {
      apiDev: {
        options: {
          wait: false
        },
        args: [
          './api_server/devApi.js'
        ]
      },
      apiDist: {
        options: {
          wait: false
        },
        args: [
          './api_server/devApi.js'
        ]
      },
      testFlow: {
        options: {
          wait: true
        },
        args: [
          './runnerFlowTests.js'
        ]
      },
      kss: {
        options: {
          wait: true
        },
        args: [
          './node_modules/kss/bin/kss-node',
          '.tmp/doc',
          'dist/styleguide',
          '--css',
          '.tmp/doc/main.css'
        ]
      }
    }
  });

  grunt.registerTask('build', function() {
    grunt.task.run([
      'clean:dist',
      'jshint:all',
      'wiredep',
      'useminPrepare',
      'copy:dist',
      'sass:dist',
      'imagemin',
      'concat',
      'uglify',
      'usemin',
      'htmlmin'
    ]);
  });

  grunt.registerTask('devCompile', function() {
    grunt.task.run([
      'clean:dev',
      'wiredep',
      'copy:devSass',
      'sass:dev',
      'rename:moveSCSS',
      'jshint:all'
    ]);
  });


  grunt.registerTask('cssDoc', function() {
    grunt.task.run([
      'sass:doc',
      'copy:docSass',
      'run:kss'
    ]);
  });

  grunt.registerTask('serve', function() {
    grunt.task.run([
      'devCompile',
      'run:apiDev',
      'configureProxies:server',
      'connect:livereload',
      'watch'

    ]);
  });

  grunt.registerTask('dist', function() {
    grunt.task.run([
      'build',
      'run:apiDist',
      'configureProxies:server',
      'connect:dist',
      'cssDoc',
      'run:testFlow',
      'rename:moveResultFlow'
    ]);
  });

  grunt.registerTask('serveDist', function() {
    grunt.task.run([
      'build',
      'run:apiDist',
      'configureProxies:server',
      'connect:distServe'
    ]);
  });

  grunt.registerTask('default', ['serve']);
};