var webApp = require('../../../../../test/flows/webApp')(casper);

//Workaround for capturing images
casper.removeAllListeners('resource.requested');

flow('Login - Validaciones', function() {
	step('Acceso a login', stepAccesoLogin);
	step('Error campos obligatorios vacíos', stepLoginCamposVacios);
});


function stepAccesoLogin() {
	webApp.toLogin(function() {
		phantomCSS.screenshot('body');
		casper.test.pass('Login accesible');
	}, function() {
		casper.test.fail('Login no accesible');
	});
}

function stepLoginCamposVacios() {
	webApp.login('', '', function() {
		phantomCSS.screenshot('body');
		casper.test.assert(webApp.loginHaveErrorMsg(),
			'Se esperaba que se mostrara un error');
	});
}