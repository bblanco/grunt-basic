module.exports = function(casper) {
	return {
		toLogin: function(callback, fail) {
			casper.thenOpen('http://localhost:9000/', function() {
				casper.waitForSelector('.login-bg', function() {
					callback();
				}, function() {
					fail();
				}, 5000);
			});
		},
		login: function(user, password, callback) {
			casper.fill('form[name=frmLogin]', {
				'user': user,
				'password': password
			}, false);

			casper.thenClick('form[name=frmLogin] button', callback);
		},
		loginHaveErrorMsg: function() {
			return casper.exists('form[name=frmLogin] .alert-error');
		}
	}
};