'use strict';

(function() {

	var app = angular.module('vacunasApp', ['ui.router']);

	app.run()
		.config(['$httpProvider',
			function($httpProvider) {
				$httpProvider.interceptors.push(['HttpApiInterceptor',
					function($httpApiInterceptor) {
						return $httpApiInterceptor;
					}
				]);
			}
		])
		.config(['$stateProvider', '$urlRouterProvider',
			function($stateProvider, $urlRouterProvider) {
				$urlRouterProvider.otherwise('/');

				$stateProvider.state('login', {
					url: '/login',
					templateUrl: 'views/login.html',
					controller: 'LoginCtrl'
				});

				$stateProvider.state('home', {
					url: '/',
					templateUrl: 'views/login.html',
					controller: 'LoginCtrl'
				});

			}
		]);

})();