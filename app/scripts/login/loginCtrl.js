'use strict';

angular.module('vacunasApp')
	.controller('LoginCtrl', ['$scope', '$http', 'SecurityService',
		function($scope, $http, securityService) {
			$scope.user = '';
			$scope.password = '';
			$scope.msgError = '';

			$scope.login = function() {

				if (this.frmLogin.$valid) {
					$scope.msgError = '';
					securityService
						.login(this.user, this.password)
						.then(function() {
						}, function(resp) {
							$scope.msgError = resp.data.error;
						});
				} else {
					$scope.msgError = 'El usuario y la contraseña son obligatorios';
				}

			};

		}
	]);