'use strict';

angular.module('vacunasApp')
	.service('SecurityService', ['$http',
		function($http) {

			this.login = function(user, password) {
				return $http.post('/api/login', {
					user: user,
					password: password
				});
			};

		}
	]);