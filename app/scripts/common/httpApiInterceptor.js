'use strict';

angular.module('vacunasApp')
	.service('HttpApiInterceptor', ['$q',
		function($q) {

			this.response = function (response) {
				if (response.data && response.data.error) {
					return $q.reject(response);
				} else {
					return response || $q.when(response);
				}
			};
		}
	]);