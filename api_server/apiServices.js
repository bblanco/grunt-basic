var apiServer = require('./apiServer');

var operators = [];
apiServer.app.post('/api/login', function(request, response, next) {
	var result = null;
	var operatorsFilter = operators.filter(function(item){
		return item.user == request.body.user &&
					 item.password == request.body.password;
	});

	if (operatorsFilter.length) {
		var ope = clone(operatorsFilter[0]);
		delete ope.password;
		result = JSON.stringify(ope);
	} else {
		result = generateError('Login o contraseña incorrectos');
	}
	response.end(result);
});




//Utilidades
function clone(obj) {
	return JSON.parse(JSON.stringify(obj));
}

function generateError(msg) {
	return JSON.stringify({error: msg});
}

module.exports = {
	server: apiServer,
	loadOperators: function(operatorsToLoad) {
		operators = operatorsToLoad;
	}
};