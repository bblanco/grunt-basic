var express = require('express');
var http = require('http');
var bodyParser = require('body-parser')

var app = express();


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded())

// parse application/json
app.use(bodyParser.json())


var server;

module.exports = {
	startServer: function() {
		app.post('/api/*', function(request, response, next) {
			response.writeHead(200, {'Content-Type': 'application/json'});
			next();
		});

		app.get('/api/*', function(request, response, next) {
			response.writeHead(200, {'Content-Type':'application/json'});
			next();
		});

		console.log('Api server running at http://localhost:8081/');
		server = http.createServer(app);
		server.listen(8081);
	},
	stopServer: function() {
		console.log('Stopping Api server...');
		server.close(function(){
			console.log('Stop listening');
		});
	},
	app: app
};